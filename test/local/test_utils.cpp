#include <unity.h>
#include "utils.h"
#include "utils.cpp"

void testDurationStringParsing(void) {
    TEST_ASSERT_EQUAL(691260, parseDurationString("1w 1d 1m"));
    TEST_ASSERT_EQUAL(8100, parseDurationString("2h 15m"));
}

int main(int argc, char** argv) {
    UNITY_BEGIN();
    RUN_TEST(testDurationStringParsing);
    UNITY_END();
    return 0;
}