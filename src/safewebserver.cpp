#include <Arduino.h>
#include <ArduinoJson.hpp>
#include <Base64.h>
#include <SPIFFS.h>
#include <WebServer.h>
#include <WString.h>

#include "safewebserver.hpp"
#include "aes.hpp"
#include "clock.hpp"
#include "configuration.h"
#include "mailjet.hpp"
#include "settings.hpp"
#include "time.h"
#include "templates.h"
#include "utils.h"
#include "providers/standalone.hpp"

char messages[10][100];
int messagesCount = 0;
const User ANONYMOUS_USER = User("anonymous", Role::ANONYMOUS);
const User HOLDER_ANONYMOUS_USER = User("anonymous", Role::HOLDER);

void buildMessagesString(char* dest) {
    int len = 10;
    for (int i = 0; i < messagesCount; i++) {
        len += strlen(messages[i]) + 9;
    }
    strcpy(dest, "");
    int idx = 0;
    for (int i = 0; i < messagesCount; i++) {
        strcpy((dest + idx), "<li>");
        idx += 4;
        strcpy((dest + idx), messages[i]);
        idx += strlen(messages[i]);
        strcpy((dest + idx), "</li>");
        idx += 5;
    }
    messagesCount = 0;
    dest[len] = 0;
}

String getFileContent(const char* fileName) {
    File f = SPIFFS.open(fileName, "r");
    String content;
    if (f) {
        content = f.readStringUntil(EOF);
    };
    f.close();
    return content;
}

// Yes, not thread safe at all!
void SafeWebServer::addMessage(const char* message) {
    strcpy(messages[messagesCount++], message);
}

User SafeWebServer::getCurrentUser(AsyncWebServerRequest* request) {
    if (strlen(eepromValues.holderUsername) == 0) {
        return HOLDER_ANONYMOUS_USER;

    } else if (request->hasHeader("Cookie")) {
        String cookie = request->header("Cookie");
        char cipheredToken[256];
        strcpy(cipheredToken, cookie.substring(6).c_str());
        if (strlen(cipheredToken) > 0) {
            char tokenJSON[256];
            decipher(safeWebServer._aes, tokenJSON, cipheredToken);
            if (isValidAscii(tokenJSON)) {
                ArduinoJson::DynamicJsonDocument doc(256);
                deserializeJson(doc, tokenJSON);
                return User(doc["username"], doc["role"]);
            }
        }
    }
    return ANONYMOUS_USER;
}

void SafeWebServer::_checkRole(User user, Role role, const char* urlRedirect, AsyncWebServerRequest* request) {
    if (strlen(eepromValues.holderUsername) > 0 &&
        !user.hasRole(role)) {
        request->redirect(String("/login?redirect=") + urlRedirect);
    }
}

User SafeWebServer::_loadUser(const char* username, const char* password) {
    byte buff[32];
    char hashedPassword[65];
    hashPassword(buff, password);
    bytesToHex(hashedPassword, buff, 32);

    if (strcmp(eepromValues.holderUsername, username) == 0 && strcmp(eepromValues.holderPassword, hashedPassword) == 0) {
        return User(username, Role::HOLDER);
    } else if (strcmp(eepromValues.lockeeUsername, username) == 0 && strcmp(eepromValues.lockeePassword, hashedPassword) == 0) {
        return User(username, Role::LOCKEE);
    }
    return ANONYMOUS_USER;
}

void SafeWebServer::notFound(AsyncWebServerRequest* request) {
    request->send(404, "text/plain", "Not found");
}

void SafeWebServer::handleLogout(AsyncWebServerRequest* request) {
    AsyncWebServerResponse* response = request->beginResponse(302, "text/plain", "\n\n");
    response->addHeader("Set-Cookie", String("token="));
    response->addHeader("Cacherequest-Control", "no-cache");
    response->addHeader("Location", "/");
    request->send(response);
}

void SafeWebServer::handleLogin(AsyncWebServerRequest* request) {
    String content = getFileContent("/login.html");
    String redirect = request->arg("redirect");
    if (redirect.length() == 0)
        redirect = "/";
    content.replace("{{redirect}}", redirect);
    AsyncWebServerResponse* response = request->beginResponse(200, "text/html", content.c_str());
    response->addHeader("Cache-Control", "no-cache");
    request->send(response);
}

void SafeWebServer::handleDoLogin(AsyncWebServerRequest* request) {
    String username = request->arg("username");
    String password = request->arg("password");
    User user = safeWebServer._loadUser(username.c_str(), password.c_str());
    AsyncWebServerResponse* response = request->beginResponse(302, "text/html", "");

    if (!user.hasRole(Role::ANONYMOUS)) {
        String token = "{'time':'" + String(mclock.getTime()) +
            "', 'username':'" + username +
            "','role':'" + user.getRole() + "'}";
        char cipheredToken[200];
        cipher(safeWebServer._aes, cipheredToken, token.c_str());
        String cookie = String("token=") + String(cipheredToken);
        response->addHeader("Set-Cookie", cookie);
        response->addHeader("Location", request->arg("redirect"));

        if (user.hasRole(Role::HOLDER)) {
            eepromValues.lastLoggedIn = mclock.getTime();
            writeEEPROM();
        }
    } else {
        response->addHeader("Location", "/login");
    }
    request->send(response);
}

void SafeWebServer::handleHome(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    String content = getFileContent("/home.html");

    if (!user.hasRole(Role::ANONYMOUS)) {
        content.replace("{{displayLogin}}", "none");
        content.replace("{{displayLogout}}", "inline");
    } else {
        content.replace("{{displayLogin}}", "inline");
        content.replace("{{displayLogout}}", "none");
    }
    if (eepromValues.standaloneMode) {
        content.replace("{{emlaDisplay}}", "none");
        content.replace("{{standaloneDisplay}}", "block");
    } else {
        content.replace("{{emlaDisplay}}", "block");
        content.replace("{{standaloneDisplay}}", "none");
    }

    char messages[1000];
    buildMessagesString(messages);
    content.replace("{{messages}}", messages);
    replaceVariables(content, user);
    request->send(200, "text/html", content.c_str());
}

void SafeWebServer::handleSetAuthentication(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    SafeWebServer::_checkRole(user, Role::HOLDER, "/setAuthentication", request);
    String content = getFileContent("/authentication.html");
    request->send(200, "text/html", content.c_str());
}

void SafeWebServer::handleDoSetAuthentication(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    SafeWebServer::_checkRole(user, Role::HOLDER, "/setAuthentication", request);
    String username = request->arg("username");
    String password = request->arg("password");

    if (username.length() == 0 || password.length() == 0) {
        *(eepromValues.holderUsername) = 0;
        *(eepromValues.holderPassword) = 0;
        eepromValues.passwordChangeTime = 0;
    } else {
        strcpy(eepromValues.holderUsername, username.c_str());
        byte buff[32];
        hashPassword(buff, password.c_str());
        bytesToHex(eepromValues.holderPassword, buff, 32);
        eepromValues.passwordChangeTime = mclock.getTime();
    }
    writeEEPROM();
    request->redirect("/");
}

void SafeWebServer::handleSettings(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    String content = getFileContent("/settings.html");
    replaceVariables(content, user);
    request->send(200, "text/html", content.c_str());
}

void SafeWebServer::handleStandalone(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    SafeWebServer::_checkRole(user, Role::HOLDER, "/standalone", request);
    String content = getFileContent("/standalone.html");
    replaceVariables(content, user);
    request->send(200, "text/html", content.c_str());
}

void SafeWebServer::handleSetStandalone(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    SafeWebServer::_checkRole(user, Role::HOLDER, "/standalone", request);
    if (eepromValues.standaloneLockEnd <= 0) {
        eepromValues.standaloneMode = request->arg("isStandaloneMode").compareTo("on") == 0;
    } else {
        eepromValues.standaloneMode = 1;
    }
    eepromValues.standaloneModeLocked = request->arg("isStandaloneModeLocked").compareTo("on") == 0;
    eepromValues.standaloneModeHiddenTimer = request->arg("isStandaloneModeHiddenTimer").compareTo("on") == 0;
    eepromValues.notifyLockeeTimerIsAboutToExpire = request->arg("notifyLockeeTimerIsAboutToExpire").compareTo("on") == 0;
    eepromValues.notifyLockeeExpiredTimer = request->arg("notifyLockeeExpiredTimer").compareTo("on") == 0;
    eepromValues.notifyLockeeTimeAdded = request->arg("notifyLockeeTimeAdded").compareTo("on") == 0;
    strcpy(eepromValues.emailNotifications, request->arg("emailNotifications").c_str());
    applyDuration(request->arg("standaloneModeAddTime"));
    applyRandomDuration(request->arg("standaloneModeAddRandomTimeFrom"), request->arg("standaloneModeAddRandomTimeTo"), true);

    eepromValues.standaloneModeAutomaticUnlock = request->arg("isStandaloneModeAutomaticUnlock").compareTo("on") == 0;
    eepromValues.standaloneModeOpeningRisk = atoi(request->arg("standaloneModeOpeningRisk").c_str());
    eepromValues.standaloneModeOpeningRiskIncrPeriod = atoi(request->arg("standaloneModeOpeningRiskIncrPeriod").c_str());
    eepromValues.standaloneModeTryingRisk = atoi(request->arg("standaloneModeTryingRisk").c_str());
    strcpy(eepromValues.standaloneModeRiskTime, request->arg("standaloneModeRiskTime").c_str());
    strcpy(eepromValues.standaloneModeRiskRandomTimeFrom, request->arg("standaloneModeRiskRandomTimeFrom").c_str());
    strcpy(eepromValues.standaloneModeRiskRandomTimeTo, request->arg("standaloneModeRiskRandomTimeTo").c_str());

    writeEEPROM();
    request->redirect("/");
}

void SafeWebServer::handleLockee(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    SafeWebServer::_checkRole(user, Role::LOCKEE, "/lockee", request);
    if (eepromValues.standaloneMode) {
        String content = getFileContent("/lockee.html");
        replaceVariables(content, user);
        request->send(200, "text/html", content.c_str());
    } else {
        request->send(200, "text/html", "Standalone mode deactivated");
    }
}

void SafeWebServer::handleSetLockee(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    SafeWebServer::_checkRole(user, Role::LOCKEE, "/lockee", request);
    if (request->arg("isStandaloneModeLocked").compareTo("on") == 0)
        eepromValues.standaloneModeLocked = 1;
    if (eepromValues.standaloneLockEnd <= 0) {
        eepromValues.standaloneModeHiddenTimer = request->arg("isStandaloneHiddenTimer").compareTo("on") == 0;
    } else if (request->arg("isStandaloneHiddenTimer").compareTo("on") == 0) {
        eepromValues.standaloneModeHiddenTimer = 1;
    }

    if (!eepromValues.standaloneModeLocked) { // else lockee could add a short timer to be free himself while the safe was manually locked!
        applyDuration(request->arg("standaloneModeAddTime"));
        applyRandomDuration(request->arg("standaloneModeAddRandomTimeFrom"), request->arg("standaloneModeAddRandomTimeTo"), true);
    }
    strcpy(eepromValues.emailNotificationsLockee, request->arg("standaloneNotificationsEmailLockee").c_str());

    writeEEPROM();
    request->redirect("/");
}

void SafeWebServer::handleSetLockeePassword(AsyncWebServerRequest* request) {
}

void SafeWebServer::handleDoSetLockePassword(AsyncWebServerRequest* request) {
}

void SafeWebServer::handleOpen(AsyncWebServerRequest* request) {
    User user = getCurrentUser(request);
    SafeWebServer::_checkRole(user, Role::LOCKEE, "/lockee", request);
    digitalWrite(PIN_OPEN_BUTTON, HIGH);
    request->redirect("/");
}

void SafeWebServer::begin(unsigned char* aesKey) {
    this->_server.begin();
    this->_aes = initCipher(eepromValues.aesKey);
}

SafeWebServer::SafeWebServer() : _server(HTTP_PORT) {
    this->_server.on("/", SafeWebServer::handleHome);
    this->_server.on("/setAuthentication", SafeWebServer::handleSetAuthentication);
    this->_server.on("/doSetAuthentication", SafeWebServer::handleDoSetAuthentication);
    this->_server.on("/setLockeePassword", SafeWebServer::handleSetLockeePassword);
    this->_server.on("/doSetLockeePassword", SafeWebServer::handleDoSetLockePassword);
    this->_server.on("/login", SafeWebServer::handleLogin);
    this->_server.on("/doLogin", SafeWebServer::handleDoLogin);
    this->_server.on("/logout", SafeWebServer::handleLogout);
    this->_server.on("/settings", SafeWebServer::handleSettings);
    this->_server.on("/standalone", SafeWebServer::handleStandalone);
    this->_server.on("/setStandalone", SafeWebServer::handleSetStandalone);
    this->_server.on("/lockee", SafeWebServer::handleLockee);
    this->_server.on("/setLockee", SafeWebServer::handleSetLockee);
    this->_server.on("/open", SafeWebServer::handleOpen);
    this->_server.serveStatic("/style.css", SPIFFS, "/style.css");
    this->_server.serveStatic("/js/notifications.js", SPIFFS, "/js/notifications.js");
    this->_server.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico");
    this->_server.serveStatic("/robots.txt", SPIFFS, "/robots.txt");
    this->_server.onNotFound(SafeWebServer::notFound);

    // const char* headersToCollect[] = { "Cookie" };
    // this->_server.collectHeaders(headersToCollect, 1);
}

SafeWebServer safeWebServer;