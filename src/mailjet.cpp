#include <Arduino.h>
#include <ArduinoJson.hpp>
#include <HTTPClient.h>

#include "configuration.h"
#include "mailjet.hpp"

void sendMail(const char* to, const char* subject, const char* message) {
    if (strlen(to) > 0 && strlen(MAILJET_USER) > 0 && strlen(MAILJET_PASSWORD) > 0) {
        Serial.println("Sending mail");

        ArduinoJson::DynamicJsonDocument doc(1024);
        doc["Messages"][0]["From"]["Email"] = "safe@justus.link";
        doc["Messages"][0]["From"]["Name"] = "Safe";
        doc["Messages"][0]["To"][0]["Email"] = to;
        doc["Messages"][0]["To"][0]["Name"] = "Safe";
        doc["Messages"][0]["Subject"] = subject;
        doc["Messages"][0]["TextPart"] = message;
        // doc["SandboxMode"] = "true";

        HTTPClient httpClient;
        httpClient.begin(MAILJET_URL);
        httpClient.setAuthorization(MAILJET_USER, MAILJET_PASSWORD);
        String json;
        serializeJson(doc, json);
        httpClient.POST(json);
    }
}