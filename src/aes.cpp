#include <AES.h>
#include <Arduino.h>
#include <WString.h>

#include "clock.hpp"
#include "settings.hpp"
#include "utils.h"

AES256 initCipher(const unsigned char* key) {
    AES256 aes = AES256();
    aes.setKey(key, 32);
    return aes;
}

/**
 * Cipher a token using AES.
 * The result is in hex format.
 */
void cipher(AES256 aes, char* buf, const char* text) {
    int len = strlen(text);
    int blockCount = len / 16;
    if (len % 16 > 0)
        blockCount++;
    uint8_t* input = (uint8_t*)malloc(blockCount * 16 + 1);
    uint8_t* output = (uint8_t*)malloc(blockCount * 16 + 1);
    input[blockCount * 16] = 0;
    output[blockCount * 16] = 0;

    for (int i = 0; i < len;i++) {
        input[i] = text[i];
    }
    for (int i = len; i < blockCount * 16;i++) {
        input[i] = ' ';
    }

    // Serial.printf("'%s'\n", input);
    for (int i = 0; i < blockCount; i++) {
        aes.encryptBlock((output + i * 16), (input + i * 16));
    }

    bytesToHex(buf, output, (blockCount * 16));
    free(input);
    free(output);
}

/**
 * Decipher a token using AES.
 */
void decipher(AES256 aes, char* buf, const char* hexInput) {
    int blockCount = strlen(hexInput) / 2 / 16;
    if ((strlen(hexInput) / 2) % 16 > 0)
        blockCount++;

    uint8_t* input = (uint8_t*)malloc(blockCount * 16 + 1);
    uint8_t* output = (uint8_t*)malloc(blockCount * 16 + 1);
    input[blockCount * 16] = 0;
    output[blockCount * 16] = 0;
    buf[blockCount * 16] = 0;

    hexToBytes(input, hexInput);

    for (int i = 0; i < blockCount; i++) {
        aes.decryptBlock((output + i * 16), (input + i * 16));
    }
    memcpy(buf, output, blockCount * 16 + 1);
    free(input);
    free(output);
}
