#ifndef UTILS_H
#define UTILS_H

char* strsconcat(int count, ...);
char* formatSecondsLeftAlloc(time_t t);
char* formatSecondsLeft(char* buf, time_t t);
uint8_t* hashPassword(uint8_t* bytesOut, const char* password);
char* bytesToHex(char* buf, uint8_t* bytes, int len);
uint8_t* hexToBytes(uint8_t* buf, const char* hex);
long parseDurationString(const char* durationString);
bool isValidAscii(char* string);

#endif