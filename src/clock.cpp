#include <Arduino.h>
#include <DS1307RTC.h>
#include <WiFi.h>
#include "clock.hpp"
#include "time.h"

Clock::Clock() : Clock(DEFAULT_NTP_SERVER, DEFAULT_GMT_OFFSET_SEC, DEFAULT_DAYLIGHT_OFFSET_SEC) {}

Clock::Clock(char* ntpServer, int gmtOffsetSec, int daylightOffsetSec) {
  this->loadTimeFromRTC();
  this->setNtpServer(ntpServer);
  this->gmtOffsetSec = gmtOffsetSec;
  this->daylightOffsetSec = daylightOffsetSec;

  strcpy(this->strRTCTime, "");
  strcpy(this->strTime, "");
}

time_t Clock::getTime() {
  startNTP();
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo, 0)) {
    Serial.println(F("Failed to obtain local time"));
    return 0;
  }
  return mktime(&timeinfo);
}

char* Clock::getTimeString() {
  startNTP();
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo, 0)) {
    strcpy(this->strTime, "??");
  }
  else {
    strftime(this->strTime, 50, "%H:%M:%S", &timeinfo);
  }
  return this->strTime;
}

char* Clock::getRTCTimeString() {
  startNTP();
  if (RTC.chipPresent) {
    tmElements_t tm;
    if (RTC.read(tm)) {
      snprintf(this->strRTCTime, 50, "%02d:%02d:%02d",
        tm.Hour,
        tm.Minute,
        tm.Second
      );
    }
  }
  else {
    strcpy(this->strRTCTime, "?");
  }
  return this->strRTCTime;
}

char* Clock::getDateString() {
  startNTP();
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo, 0)) {
    strcpy(this->strDate, "??");
  }
  else {
    strftime(this->strDate, 11, "%d/%m/%Y", &timeinfo);
  }
  return this->strDate;
}

char* Clock::getRTCDateString() {
  if (RTC.chipPresent) {
    tmElements_t tm;
    if (RTC.read(tm)) {
      snprintf(this->strRTCDate, 11, "%02d/%02d/%04d",
        tm.Day,
        tm.Month,
        tmYearToCalendar(tm.Year)
      );
    }
  }
  else {
    strcpy(this->strRTCDate, "?");
  }
  return this->strRTCDate;
}

void Clock::setNtpServer(char* ntpServer) {
  this->ntpServer = ntpServer;
  this->daylightOffsetSec = DEFAULT_DAYLIGHT_OFFSET_SEC;
  this->gmtOffsetSec = DEFAULT_GMT_OFFSET_SEC;
  this->startNTP();
}

void Clock::startNTP() {
  if (!this->ntpStarted) {
    if (WiFi.isConnected()) {
      configTime(this->gmtOffsetSec, this->daylightOffsetSec, this->ntpServer);
      this->ntpStarted = true;
    }
    else {
      Serial.println(F("No WiFi connection, will not start NTP Client..."));
    }
  }
}

void Clock::saveTimeToRTC() {
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo, 0)) {
    Serial.println(F("Failed to obtain time for saveTimeToRTC"));
    return;
  }
  this->setRTCTime(timeinfo.tm_year + 1900,
    timeinfo.tm_mon + 1,
    timeinfo.tm_mday,
    timeinfo.tm_hour,
    timeinfo.tm_min,
    timeinfo.tm_sec
  );
}

void Clock::setTime(int year, int month, int day, int hour, int minutes, int seconds) {
  tmElements_t tm;
  tm.Year = CalendarYrToTm(year);
  tm.Month = month;
  tm.Day = day;
  tm.Hour = hour;
  tm.Minute = minutes;
  tm.Second = seconds;

  time_t timestamp = makeTime(tm);
  this->setRTCTime(timestamp);

  timeval epoch = { timestamp, 0 };
  settimeofday((const timeval*)&epoch, 0);
}

void Clock::setTime(time_t timestamp) {
  this->setRTCTime(timestamp);
  timeval epoch = { timestamp, 0 };
  settimeofday((const timeval*)&epoch, 0);
}

void Clock::setRTCTime(int year, int month, int day, int hour, int minutes, int seconds) {
  if (RTC.chipPresent()) {
    tmElements_t tm;
    tm.Year = CalendarYrToTm(year);
    tm.Month = month;
    tm.Day = day;
    tm.Hour = hour;
    tm.Minute = minutes;
    tm.Second = seconds;
    RTC.write(tm);
  }
}

void Clock::setRTCTime(time_t timestamp) {
  if (RTC.chipPresent()) {
    RTC.set(timestamp);
  }
}

void Clock::loadTimeFromRTC() {
  int timestamp = RTC.get();

  if (timestamp == 0) {
    if (!RTC.chipPresent()) {
      Serial.println("No RTC Chip !");
    }
    else {
      Serial.println("RTC Chip not configured. Fixing arbitrary date");
      RTC.set(DEFAULT_TIMESTAMP);
    }
  }

  timestamp = RTC.get();
  if (timestamp != 0) {
    char buf[70];
    sprintf(buf, "Loading time from RTC module %d", timestamp);
    Serial.println(buf);

    timeval epoch = { RTC.get(), 0 };
    settimeofday((const timeval*)&epoch, 0);
  }
  else {
    Serial.println("No time available");
  }
}

Clock mclock;