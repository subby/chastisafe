#ifndef CLOCK_H
#define CLOCK_H

#include <TimeLib.h>
#include "time.h"

#define DEFAULT_NTP_SERVER          "pool.ntp.org"
#define DEFAULT_GMT_OFFSET_SEC      3600
#define DEFAULT_DAYLIGHT_OFFSET_SEC 0
#define DEFAULT_TIMESTAMP           1546300800

class Clock
{
public:
  Clock();
  Clock(char* ntpServer, int gmtOffsetSec, int daylightOffsetSec);

  time_t getTime();
  void getTimeElements(tmElements_t& tmElements);
  char* getTimeString();
  char* getRTCTimeString();
  char* getDateString();
  char* getRTCDateString();

  void setTime(int year, int month, int day, int hour, int minutes, int seconds);
  void setTime(time_t timestamp);

  void saveTimeToRTC();

  void setNtpServer(char* ntpServer);

private:
  void loadTimeFromRTC();

  void setRTCTime(int year, int month, int day, int hour, int minutes, int seconds);
  void setRTCTime(time_t timestamp);
  void startNTP();

  bool ntpStarted = false;
  char* ntpServer;
  int gmtOffsetSec;
  int daylightOffsetSec;

  char strDate[11];
  char strTime[50];
  char strRTCDate[11];
  char strRTCTime[50];
};

extern Clock mclock;

#endif