#include <EEPROM.h> 
#include "configuration.h"
#include "settings.hpp"
#include "utils.h"

EepromValuesStruct eepromValues;
void writeEEPROM() {
    EEPROM.put(0, eepromValues);
    EEPROM.commit();
}

void readEEPROM() {
    EEPROM.begin(1024);
    EEPROM.get(0, eepromValues);

    Serial.printf("Found EEPROM Signature : %d\n", eepromValues.sig);

    if (eepromValues.sig != EEPROM_SIG) {
        Serial.println("Initialize EEPROM");
        eepromValues.sig = EEPROM_SIG;
        eepromValues.passwordChangeTime = 0;
        eepromValues.punishmentStartTime = 0;
        eepromValues.lastUpdate = 0;
        eepromValues.standaloneMode = true;
        eepromValues.standaloneModeLocked = false;
        eepromValues.standaloneModeHiddenTimer = false;
        eepromValues.standaloneModeAutomaticUnlock = false;
        eepromValues.standaloneModeOpeningRisk = 5;
        eepromValues.standaloneModeOpeningRiskIncrPeriod = 15;
        eepromValues.standaloneModeTryingRisk = 20;
        eepromValues.standaloneLockStart = -1;
        eepromValues.standaloneLockEnd = -1;
        eepromValues.standaloneUnlockTime = -1;
        eepromValues.lastLoggedIn = 0;
        eepromValues.lastTimeAdded = 0;
        eepromValues.lastTimeAddedRandom = 0;
        eepromValues.lastExpirationNotificationDate = 0;
        eepromValues.lastExpirationNotificationLockeeDate = 0;
        eepromValues.notifyLockeeTimerIsAboutToExpire = 0;
        eepromValues.notifyLockeeExpiredTimer = 0;
        eepromValues.notifyLockeeTimeAdded = 0;

        strcpy(eepromValues.runningSessionId, "");
        strcpy(eepromValues.standaloneModeRiskTime, "");
        strcpy(eepromValues.standaloneModeRiskRandomTimeFrom, "");
        strcpy(eepromValues.standaloneModeRiskRandomTimeTo, "");
        strcpy(eepromValues.emailNotifications, "");
        strcpy(eepromValues.emailNotificationsLockee, "");

        generateAESKey(eepromValues.aesKey);
        strcpy(eepromValues.holderUsername, "holder");
        strcpy(eepromValues.lockeeUsername, "lockee");
        uint8_t ubuf[32];
        hashPassword(ubuf, "holder");
        bytesToHex(eepromValues.holderPassword, ubuf, 32);
        hashPassword(ubuf, "lockee");
        bytesToHex(eepromValues.lockeePassword, ubuf, 32);

        Serial.println("writing eeprom");
        writeEEPROM();
    }
    Serial.println(eepromValues.holderUsername);
    Serial.println(eepromValues.holderPassword);
    Serial.println(eepromValues.lockeeUsername);
    Serial.println(eepromValues.lockeePassword);
}

void generateAESKey(unsigned char* key) {
    char chars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/*-+&#'{(|_^)$*}";
    // char chars[] = "a";
    int charsCount = strlen(chars);
    for (int i = 0; i < 32; i++) {
        key[i] = chars[random(charsCount)];
    }
    key[32] = 0;
}

void cleanSessionEEPROM() {
    eepromValues.punishmentStartTime = 0;
    strcpy(eepromValues.runningSessionId, "");
    writeEEPROM();
}