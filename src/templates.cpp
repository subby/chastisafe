
#include "safewebserver.hpp"
#include "aes.hpp"
#include "clock.hpp"
#include "configuration.h"
#include "settings.hpp"
#include "time.h"
#include "templates.h"
#include "utils.h"

void replaceVariables(String& content, User user) {
    char tmpbuf[50];
    content.replace("{{standaloneModeOpened}}", digitalRead(PIN_RELAY) == HIGH ? "yes" : "no");
    content.replace("{{emailNotifications}}", eepromValues.emailNotifications);
    content.replace("{{standaloneNotificationsEmailLockee}}", eepromValues.emailNotificationsLockee);
    content.replace("{{lastUpdate}}", String(eepromValues.lastUpdate));
    content.replace("{{locked}}", digitalRead(PIN_RELAY) ? "Unlocked" : "Locked");
    content.replace("{{loggedUser}}", user.getUsername());
    content.replace("{{notifyLockeeTimerIsAboutToExpire}}", eepromValues.notifyLockeeTimerIsAboutToExpire ? "checked" : "");
    content.replace("{{notifyLockeeExpiredTimer}}", eepromValues.notifyLockeeExpiredTimer ? "checked" : "");
    content.replace("{{notifyLockeeTimeAdded}}", eepromValues.notifyLockeeTimeAdded ? "checked" : "");
    content.replace("{{isReclaimed}}", strlen(eepromValues.holderPassword) > 0 ? "checked" : "");
    content.replace("{{isStandaloneMode}}", eepromValues.standaloneMode ? "checked" : "");
    content.replace("{{isStandaloneModeLocked}}", eepromValues.standaloneModeLocked ? "checked" : "");
    content.replace("{{isStandaloneModeHiddenTimer}}", eepromValues.standaloneModeHiddenTimer ? "checked" : "");
    content.replace("{{isStandaloneModeAutomaticUnlock}}", eepromValues.standaloneModeAutomaticUnlock ? "checked" : "");
    content.replace("{{sessionId}}", eepromValues.runningSessionId);
    content.replace("{{standaloneNotificationsEmailLockee}}", eepromValues.emailNotificationsLockee);
    content.replace("{{standaloneNotificationsEmail}}", eepromValues.emailNotifications);
    content.replace("{{holderUsername}}", eepromValues.holderUsername);
    content.replace("{{holderPassword}}", eepromValues.holderPassword);
    content.replace("{{lockeeUsername}}", eepromValues.lockeeUsername);
    content.replace("{{lockeePassword}}", eepromValues.lockeePassword);

    content.replace("{{standaloneModeOpeningRisk}}", String(eepromValues.standaloneModeOpeningRisk));
    content.replace("{{standaloneModeOpeningRiskIncrPeriod}}", String(eepromValues.standaloneModeOpeningRiskIncrPeriod));
    content.replace("{{standaloneModeTryingRisk}}", String(eepromValues.standaloneModeTryingRisk));
    content.replace("{{standaloneModeRiskTime}}", eepromValues.standaloneModeRiskTime);
    content.replace("{{standaloneModeRiskRandomTimeFrom}}", eepromValues.standaloneModeRiskRandomTimeFrom);
    content.replace("{{standaloneModeRiskRandomTimeTo}}", eepromValues.standaloneModeRiskRandomTimeTo);

    int periods = 0;
    if (eepromValues.standaloneModeOpeningRiskIncrPeriod > 0)
        periods = (mclock.getTime() - eepromValues.standaloneUnlockTime) / eepromValues.standaloneModeOpeningRiskIncrPeriod / 60;
    int risk = (periods + 1) * eepromValues.standaloneModeOpeningRisk;
    content.replace("{{standaloneModeOpeningActualRisk}}", String(risk));

    strftime(tmpbuf, 50, "%d/%m/%Y %H:%M:%S", gmtime(&eepromValues.lastLoggedIn));
    content.replace("{{lastLogin}}", tmpbuf);
    strftime(tmpbuf, 50, "%d/%m/%Y %H:%M:%S", gmtime(&eepromValues.lastTimeAdded));
    content.replace("{{lastTimeAdded}}", tmpbuf);
    strftime(tmpbuf, 50, "%d/%m/%Y %H:%M:%S", gmtime(&eepromValues.lastTimeAddedRandom));
    content.replace("{{lastTimeAddedRandom}}", tmpbuf);

    int duration = eepromValues.standaloneLockEnd - mclock.getTime();
    formatSecondsLeft(tmpbuf, duration);
    if (!user.hasRole(Role::HOLDER) && eepromValues.standaloneModeHiddenTimer) {
        content.replace("{{standaloneModeDuration}}", "Hidden");
    } else {
        content.replace("{{standaloneModeDuration}}", tmpbuf);
    }
    content.replace("{{standaloneModeDurationDebug}}", tmpbuf);
}