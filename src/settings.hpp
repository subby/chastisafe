#ifndef SETTINGS_H
#define SETTINGS_H

#define EEPROM_SIG 1002

typedef struct {
    unsigned short sig;
    char holderUsername[50];
    char holderPassword[65];
    char lockeePassword[65];
    char lockeeUsername[50];

    char runningSessionId[50];
    int passwordChangeTime;
    unsigned char aesKey[33];

    int punishmentStartTime;
    int lastUpdate;

    bool standaloneMode;
    bool standaloneModeLocked;
    bool standaloneModeHiddenTimer;
    bool standaloneModeAutomaticUnlock;
    int standaloneModeOpeningRisk;
    int standaloneModeOpeningRiskIncrPeriod;
    int standaloneModeTryingRisk;
    char standaloneModeRiskTime[25];
    char standaloneModeRiskRandomTimeFrom[25];
    char standaloneModeRiskRandomTimeTo[25];

    int standaloneLockStart;
    int standaloneLockEnd;
    int standaloneUnlockTime;

    long lastLoggedIn;
    long lastTimeAdded;
    long lastTimeAddedRandom;

    char emailNotifications[100];
    char emailNotificationsLockee[100];
    int lastExpirationNotificationDate;
    int lastExpirationNotificationLockeeDate;
    bool notifyLockeeTimerIsAboutToExpire;
    bool notifyLockeeExpiredTimer;
    bool notifyLockeeTimeAdded;
} EepromValuesStruct;

void writeEEPROM();
void readEEPROM();
void cleanSessionEEPROM();
void generateAESKey(unsigned char* key);

extern EepromValuesStruct eepromValues;

#endif