
#include <Arduino.h>
#include <time.h>
#include <SPIFFS.h>
#include <WiFi.h>
#include <WString.h>

#include "configuration.h"
#include "clock.hpp"
#include "mailjet.hpp"
#include "safewebserver.hpp"
#include "settings.hpp"
#include "utils.h"

#include "providers/emla.hpp"
#include "providers/standalone.hpp"


Emla emla(EMLA_SERVER_URL, EMLA_USER_ID, EMLA_API_KEY);

uint32_t refreshDelayMS;

void startWifi();
int  handleEmla();
int  handleManualLock(bool tryOpen);
void lock();
void unlock();

void setup() {
  pinMode(PIN_RELAY, OUTPUT);
  pinMode(PIN_OPEN_BUTTON, INPUT);

  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
  }

  Serial.begin(115200);
  while (!Serial);

  // Pause for debug, uncomment if needed
  // while (digitalRead(13) == 0) {
  //   Serial.print(".");
  //   delay(500);
  // }
  delay(1000);

  // Read eepromconfig
  readEEPROM();

  startWifi();
  safeWebServer.begin(eepromValues.aesKey);
  randomSeed(analogRead(0));
}

bool buttonLatch = false;
void loop() {
  bool tryOpen = false;
  if (!buttonLatch) {
    tryOpen = digitalRead(PIN_OPEN_BUTTON) == HIGH;
    buttonLatch = true;
  } else {
    if (digitalRead(PIN_OPEN_BUTTON) == LOW) {
      buttonLatch = false;
    }
  }

  int time = millis();
  if (time - eepromValues.lastUpdate > refreshDelayMS || tryOpen) {
    if (eepromValues.standaloneMode) {
      refreshDelayMS = handleManualLock(tryOpen);
    } else if (EMLA_ENABLED) {
      refreshDelayMS = handleEmla();
    } else {
      refreshDelayMS = DEFAULT_REFRESH_DELAY;
    }
    mclock.getTimeString();
    // Serial.print(F("Delay: "));
    // Serial.println(refreshDelayMS);
    // Serial.print(F("Clock: "));
    // Serial.println(mclock.getTimeString());
    // Serial.print(F("RTC Clock: "));
    // Serial.println(mclock.getRTCTimeString());
    // Serial.println(F("-----------------------"));
    mclock.saveTimeToRTC();

    eepromValues.lastUpdate = millis();
    writeEEPROM();
  }

  // safeWebServer.handleClient();
}

int handleManualLock(bool tryOpen) {
  // Serial.printf("Standalone mode : try: %d, ", tryOpen);
  if (eepromValues.standaloneLockStart >= 0) {
    // Serial.println("Manual timer engaged");

    if (mclock.getTime() >= eepromValues.standaloneLockEnd) {
      sendMail(eepromValues.emailNotifications, "Timer finished", "Timer finished");
      if (eepromValues.notifyLockeeExpiredTimer) {
        sendMail(eepromValues.emailNotificationsLockee, "Timer finished", "Timer finished");
      }
      // Serial.println("Timer finished");
      eepromValues.standaloneLockEnd = -1;
      eepromValues.standaloneLockStart = -1;
      eepromValues.standaloneUnlockTime = mclock.getTime();
      eepromValues.standaloneModeLocked = false;
      writeEEPROM();
    } else {
      if (eepromValues.standaloneLockEnd - mclock.getTime() < STANDALONE_WARN_END_DELAY_IN_SECONDS
        && eepromValues.lastExpirationNotificationDate <= 0) {
        eepromValues.lastExpirationNotificationDate = mclock.getTime();
        sendMail(eepromValues.emailNotifications, "Timer is about to expire", "Timer is about to expire");
      }
      if (eepromValues.standaloneLockEnd - mclock.getTime() < STANDALONE_WARN_LOCKEE_END_DELAY_IN_SECONDS
        && eepromValues.lastExpirationNotificationLockeeDate <= 0
        && eepromValues.notifyLockeeTimerIsAboutToExpire) {
        eepromValues.lastExpirationNotificationLockeeDate = mclock.getTime();
        sendMail(eepromValues.emailNotificationsLockee, "Timer is about to expire", "Timer is about to expire");
      }
      writeEEPROM();
      lock();
      if (tryOpen) {
        // APPLY RANDOM
        int chance = random(100);
        if (chance <= eepromValues.standaloneModeTryingRisk) {
          applyRandomDuration(
            String(eepromValues.standaloneModeRiskRandomTimeFrom),
            String(eepromValues.standaloneModeRiskRandomTimeTo),
            false
          );
        }
      }
    }
  } else {
    if (eepromValues.standaloneModeLocked) {
      // Serial.println("Standalone mode locked");
      lock();
    } else {
      // Serial.println("Standalone mode unlocked");
      if (eepromValues.standaloneModeAutomaticUnlock) {
        // Serial.println("Automatically open");
        unlock();
      } else if (tryOpen) {
        // Serial.println("Try open");
        if (eepromValues.standaloneModeOpeningRisk > 0 && eepromValues.standaloneUnlockTime >= 0) {
          // APPLY RANDOM    
          int periods = 0;
          if (eepromValues.standaloneModeOpeningRiskIncrPeriod > 0)
            periods = (mclock.getTime() - eepromValues.standaloneUnlockTime) / eepromValues.standaloneModeOpeningRiskIncrPeriod / 60;
          int risk = (periods + 1) * eepromValues.standaloneModeOpeningRisk;
          int chance = random(100);
          if (chance <= risk) {
            eepromValues.standaloneLockEnd = -1;
            applyDuration(String(eepromValues.standaloneModeRiskTime));
            applyRandomDuration(
              String(eepromValues.standaloneModeRiskRandomTimeFrom),
              String(eepromValues.standaloneModeRiskRandomTimeTo),
              false
            );
          } else {
            // Serial.println("Open");
            sendMail(eepromValues.emailNotifications, "Safe opened", "Safe has been opened !!! Hooo nooo !");
            eepromValues.standaloneUnlockTime = -1;
            unlock();
          }
        } else {
          // Serial.println("Open");
          eepromValues.standaloneUnlockTime = -1;
          unlock();
        }
        writeEEPROM();
      }
    }
  }
  return STANDALONE_MODE_REFRESH_DELAY;
}

void startWifi() {
  if (WiFi.status() != WL_CONNECTED) {
    uint8_t timeSpent = 0;

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    Serial.printf("Connection WiFi to %s", WIFI_SSID);
    while (WiFi.status() != WL_CONNECTED && timeSpent < WIFI_CONNECT_DELAY) {
      delay(500);
      timeSpent += 500;
      Serial.print(F("."));
    }
    Serial.println(F(""));
    Serial.println(F("WiFi connected"));
    Serial.print(F("IP address: "));
    Serial.println(WiFi.localIP());
  }
}

int handleEmla() {
  int refreshDelay = DEFAULT_REFRESH_DELAY;
  emla.refreshData(mclock.getTime());

  bool locked = emla.isLocked(mclock.getTime());
  if (locked) {
    refreshDelay = EMLA_REFRESH_DELAY_LOCKED_MS;
    lock();
  } else {
    refreshDelay = EMLA_REFRESH_DELAY_UNLOCKED_MS;
    cleanSessionEEPROM();
    unlock();
  }
  Serial.printf("Session: %s\n", emla.getSessionId());
  Serial.printf("Status: %d\n", emla.getStatus());
  Serial.printf("Locked: %d\n", locked);

  strcpy(eepromValues.runningSessionId, emla.getSessionId());

  return refreshDelay;
}

void lock() {
  if (digitalRead(PIN_RELAY) == HIGH) {
    Serial.println(F("Lock"));
    digitalWrite(PIN_RELAY, LOW);
  }
}

void unlock() {
  if (digitalRead(PIN_RELAY) == LOW) {
    Serial.println(F("unlock"));
    digitalWrite(PIN_RELAY, HIGH);
  }
}