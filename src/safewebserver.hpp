#ifndef SAFEWEBSERVER_H
#define SAFEWEBSERVER_H

#include <aes.hpp>
#include <ESPAsyncWebServer.h>

enum Role { ANONYMOUS, HOLDER, LOCKEE };

class User {
public:
    User(const char* username, Role role) {
        strcpy(this->_username, username);
        this->_role = role;
    }

    const char* getUsername() {
        return this->_username;
    }

    Role getRole() {
        return this->_role;
    }

    bool hasRole(Role role) {
        return this->_role == role;
    }

    String toString() {
        return String(_username) + ":" + String(_role);
    }
private:
    char _username[50];
    Role _role;
};

class SafeWebServer
{

public:
    SafeWebServer();
    void begin(unsigned char* aesKey);
    void addMessage(const char* message);
    static User getCurrentUser(AsyncWebServerRequest* request);

private:
    AsyncWebServer _server;

    User _loadUser(const char* user, const char* password);

    static void _checkRole(User user, Role role, const char* urlRedirect, AsyncWebServerRequest* request);

    static void notFound(AsyncWebServerRequest* request);
    static void handleDoLogin(AsyncWebServerRequest* request);
    static void handleHome(AsyncWebServerRequest* request);
    static void handleDoSetAuthentication(AsyncWebServerRequest* request);
    static void handleSetAuthentication(AsyncWebServerRequest* request);
    static void handleSetLockeePassword(AsyncWebServerRequest* request);
    static void handleDoSetLockePassword(AsyncWebServerRequest* request);
    static void handleLockee(AsyncWebServerRequest* request);
    static void handleLogin(AsyncWebServerRequest* request);
    static void handleLogout(AsyncWebServerRequest* request);
    static void handleSettings(AsyncWebServerRequest* request);
    static void handleStandalone(AsyncWebServerRequest* request);
    static void handleSetLockee(AsyncWebServerRequest* request);
    static void handleSetStandalone(AsyncWebServerRequest* request);
    static void handleOpen(AsyncWebServerRequest* request);

    AES256 _aes;
};

extern SafeWebServer safeWebServer;

#endif