#include <Arduino.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <TimeLib.h>
#include <Crypto.h>
#include <SHA3.h>

char* strsconcat(int count, ...) {
    int len = 0;
    va_list args;
    va_start(args, count);
    for (int i = 0; i < count; i++) {
        const char* s = va_arg(args, const char*);
        if (s != NULL) {
            len += strlen(s);
        }
    }
    va_end(args);

    char* result = (char*)malloc(len + 1);
    len = 0;

    va_start(args, count);
    for (int i = 0; i < count; i++) {
        const char* s = va_arg(args, const char*);
        if (s != NULL) {
            strcpy(result + len, s);
            len += strlen(s);
        }
    }
    va_end(args);
    result[len + 1] = 0;
    return result;
}

char* formatSecondsLeft(char* buf, time_t t) {
    if (t < 0)
        t = 0;
    int days = elapsedDays(t);
    int hours = numberOfHours(t);
    int minutes = numberOfMinutes(t);
    int seconds = numberOfSeconds(t);
    sprintf(buf, "%dd %02d:%02d:%02d", days, hours, minutes, seconds);
    return buf;
}

char* formatSecondsLeftAlloc(time_t t) {
    char* buf = (char*)malloc(20);
    return formatSecondsLeft(buf, t);
}

/**
 * Hash a password using SHA3_256 into a 32 bytes array
 */
void hashPassword(uint8_t* bytesOut, const char* password) {
    SHA3_256 sha3_256;
    sha3_256.update(password, strlen(password));
    sha3_256.finalize(bytesOut, 32);
}

void bytesToHex(char* buf, uint8_t* bytes, int len) {
    for (int i = 0; i < len; i++) {
        sprintf((buf + i * 2), "%02X", bytes[i]);
    }
    buf[len * 2] = '\0';
}

void hexToBytes(uint8_t* buf, const char* hex) {
    for (int i = 0; i < strlen(hex) / 2; i++) {
        char val[3] = { hex[i * 2], hex[i * 2 + 1], 0 };
        buf[i] = strtol(val, NULL, 16);
    }
}

/**
 * Parse a duration string using the following format :
 * nW nD nH nM
 *
 * and returns a duration in seconds
 **/
long parseDurationString(const char* durationString) {
    long duration = 0;
    int lastSpace = 0;
    int valueSize = 0;
    char valueStr[20];
    for (int i = 0; i < strlen(durationString) + 1; i++) {
        if (durationString[i] == ' ' || durationString[i] == 0) {
            valueStr[valueSize] = 0;
            char unit = durationString[i - 1];
            int value = atoi(valueStr);

            int mult = 1;
            switch (unit) {
            case 'w':
            case 'W':
                mult = 604800;
                break;
            case 'd':
            case 'D':
                mult = 86400;
                break;
            case 'h':
            case 'H':
                mult = 3600;
                break;
            case 'm':
            case 'M':
                mult = 60;
                break;
            }
            duration += value * mult;
            valueSize = 0;
        } else {
            valueStr[valueSize++] = durationString[i];
        }
    }
    return duration;
}

bool isValidAscii(char* string) {
    for (int i = 0; i < strlen(string);i++) {
        if (!isprint(string[i]))
            return false;
    }
    return true;
}