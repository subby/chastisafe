#ifndef SAFEAES_H
#define SAFEAES_H

#include <AES.h>

AES256 initCipher(const unsigned char* key);
char* cipher(AES256 aes, char* buf, const char* user);
char* decipher(AES256 aes, char* buf, const char* hexInput);

#endif