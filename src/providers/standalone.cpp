#include <Arduino.h>
#include <WString.h>

#include "safewebserver.hpp"
#include "standalone.hpp"
#include "clock.hpp"
#include "configuration.h"
#include "mailjet.hpp"
#include "settings.hpp"
#include "time.h"
#include "utils.h"

void applyDuration(String durationString) {
    long duration = parseDurationString(durationString.c_str());
    if (duration > 0) {
        eepromValues.standaloneLockStart = mclock.getTime();
        if (eepromValues.standaloneLockEnd <= 0)
            eepromValues.standaloneLockEnd = eepromValues.standaloneLockStart;
        // check long capacity
        if (LONG_MAX - eepromValues.standaloneLockEnd > duration) {
            eepromValues.standaloneLockEnd += duration;
        } else {
            eepromValues.standaloneLockEnd = LONG_MAX;
        }

        eepromValues.lastTimeAdded = mclock.getTime();
        eepromValues.lastExpirationNotificationDate = 0;

        if (eepromValues.notifyLockeeTimeAdded) {
            sendMail(eepromValues.emailNotificationsLockee, "Time added", "Fixed time added");
        }
        String msg = "Fixed time added : " + durationString;
        sendMail(eepromValues.emailNotifications, "Time added", msg.c_str());
    }
}

void applyRandomDuration(String durationFromString, String durationToString, bool showMessages) {
    long randomFrom = parseDurationString(durationFromString.c_str());
    long randomTo = parseDurationString(durationToString.c_str());
    if (randomTo > 0) {
        long duration = randomWithRisk(randomFrom, randomTo, showMessages);
        eepromValues.standaloneLockStart = mclock.getTime();
        if (eepromValues.standaloneLockEnd <= 0)
            eepromValues.standaloneLockEnd = eepromValues.standaloneLockStart;

        // check long capacity
        if (LONG_MAX - eepromValues.standaloneLockEnd > duration) {
            eepromValues.standaloneLockEnd += duration;
        } else {
            eepromValues.standaloneLockEnd = LONG_MAX;
        }

        eepromValues.lastTimeAddedRandom = mclock.getTime();
        eepromValues.lastExpirationNotificationDate = 0;

        if (eepromValues.notifyLockeeTimeAdded) {
            sendMail(eepromValues.emailNotificationsLockee, "Time added", "Random time added");
        }
        char buf[50];
        formatSecondsLeft(buf, duration);
        String msg = String("Random time added : ") + buf;
        sendMail(eepromValues.emailNotifications, "Time added", msg.c_str());
    }
}

int randomWithRisk(long randomFrom, long randomTo, bool showMessages) {
    long duration = random(randomFrom, randomTo);
    int risk = random(100);
    char buf[20];
    char message[100];
    sprintf(message, "Random time added is %s", formatSecondsLeft(buf, duration));
    if (showMessages) {
        safeWebServer.addMessage(message);
        message[0] = 0;
    }

    if (risk == 1) {
        sprintf(message, "Oups... Epic fail ! using max time * %f", STANDALONE_RISK_FACTOR);
    } else if (risk == 100) {
        duration *= 0.5;
        strcpy(message, "Lucky lockee... time halved");
    } else if (risk <= STANDALONE_RISK_CHANCE) { 
        duration *= STANDALONE_RISK_FACTOR;
        sprintf(message, "Oups... bad luck, risk factor applied. Random time x %f", STANDALONE_RISK_FACTOR);
    }
    if (showMessages && strlen(message) > 0) {
        safeWebServer.addMessage(message);
    }
    return duration;
}