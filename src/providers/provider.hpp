class SessionProvider
{
public:
    SessionProvider();

    int refresh();
    bool isLocked();
};
