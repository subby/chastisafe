#include <Arduino.h>
#include <ArduinoJson.hpp>
#include <stdio.h>

#include "configuration.h"
#include "emla.hpp"
#include "settings.hpp"
#include "utils.h"

#define EMLA_SESSION_STATUS_RUNNING 1
#define EMLA_SESSION_STATUS_COMPLETED 2
#define EMLA_SESSION_STATUS_EMERGENCY 3
#define EMLA_SESSION_STATUS_FAILED 4

const char* EMLA_JSON_CHASTITYSESSION = "chastitysession";
const char* EMLA_JSON_CHASTITYSESSIONID = "chastitysessionid";

Emla::Emla(const char* serverURL, const char* userId, const char* apiKey) {
    this->serverURL = serverURL;
    this->userId = userId;
    this->apiKey = apiKey;

    this->httpClient = new HTTPClient();
}

void Emla::refreshData(int timestamp) {
    if (strlen(this->sessionId) == 0) {
        if (strlen(eepromValues.runningSessionId) > 0) {
            strncpy(this->sessionId, eepromValues.runningSessionId, 30);
        }
        else {
            getUserSession();
        }
    }

    if (strlen(this->sessionId) > 0) {
        refreshSession(this->sessionId);
    }
    this->lastUpdate = timestamp;
}

void Emla::refreshSession(const char* sessionId) {
    Serial.printf("refresh session %s\n", sessionId);
    char parameters[50];
    sprintf(parameters, "&%s=%s", EMLA_JSON_CHASTITYSESSIONID, sessionId);
    char* url = buildURL("/chastitysession", parameters);

    this->httpClient->begin(url);
    int httpResponseCode = this->httpClient->GET();
    if (httpResponseCode == 200) {
        String payload = this->httpClient->getString();
        ArduinoJson::DynamicJsonDocument doc(4096);
        deserializeJson(doc, payload);
        Serial.println(payload);
        if (doc.containsKey(EMLA_JSON_CHASTITYSESSIONID)) {
            this->status = doc["status"];
            this->startTimestamp = doc["startdate"];
            this->endTimestamp = doc["enddate"];
            this->minDuration = doc["minduration"];
            this->maxDuration = doc["maxduration"];
        }
    }
    else {
        Serial.printf("Error code: %d", httpResponseCode);
    }
    this->httpClient->end();
}

void Emla::getUserSession() {
    Serial.println("getUserSession");
    char* url = buildURL("/info");

    this->httpClient->begin(url);
    int httpResponseCode = this->httpClient->GET();
    if (httpResponseCode == 200) {
        String payload = this->httpClient->getString();

        ArduinoJson::DynamicJsonDocument doc(4096);
        deserializeJson(doc, payload);
        if (doc.containsKey(EMLA_JSON_CHASTITYSESSION) &&
            doc[EMLA_JSON_CHASTITYSESSION].containsKey(EMLA_JSON_CHASTITYSESSIONID)) {

            strncpy(this->sessionId, doc[EMLA_JSON_CHASTITYSESSION][EMLA_JSON_CHASTITYSESSIONID], 30);
            strncpy(eepromValues.runningSessionId, doc[EMLA_JSON_CHASTITYSESSION][EMLA_JSON_CHASTITYSESSIONID], 30);
            writeEEPROM();
        }
        else {
            Serial.println("no session linked to user");
        }
    }
    else {
        Serial.printf("Error code: %d", httpResponseCode);
    }
    this->httpClient->end();
}

bool Emla::isCanceled() {
    return this->status == EMLA_SESSION_STATUS_FAILED;
}

bool Emla::isLocked(int timestamp) {
    if (this->status == EMLA_SESSION_STATUS_RUNNING) {
        Serial.println("Session is running");
        if (this->lastUpdate > 0
            && timestamp > 0
            && timestamp - this->lastUpdate > EMLA_FAILSAFE_IN_MINUTES * 60) {
            strcpy(this->sessionId, "");
            cleanSessionEEPROM();
            return false;
        }
        return true;
    }
    else if (this->status >= EMLA_SESSION_STATUS_FAILED) {
        if (eepromValues.punishmentStartTime == 0) {
            Serial.println("Session failed applying punishment");
            eepromValues.punishmentStartTime = timestamp;
            writeEEPROM();
            return true;
        }
        else {
            int timeLeft = eepromValues.punishmentStartTime + EMLA_PUNISHMENT_AFTER_CANCEL_IN_MINUTES * 60 - timestamp;
            Serial.printf("Session failed remaining %d seconds on punishment\n", timeLeft);
            bool locked = (timeLeft > 0);
            if (!locked) {
                strcpy(this->sessionId, "");
                cleanSessionEEPROM();
            }
            return locked;
        }
    }
    else {
        Serial.println("Unkown status");
    }
    strcpy(this->sessionId, "");
    cleanSessionEEPROM();
    return false;
}

const char* Emla::getSessionId() {
    return this->sessionId;
}

int Emla::getStatus() {
    return this->status;
}

char* Emla::buildURL(const char* path) {
    return strsconcat(6, this->serverURL, path, F("?userid="), this->userId, F("&apikey="), this->apiKey);
}

char* Emla::buildURL(const char* path, char* parameters) {
    return strsconcat(7, this->serverURL, path, F("?userid="), this->userId, F("&apikey="), this->apiKey, parameters);
}