#include <HTTPClient.h>

class Emla
{
public:
    Emla(const char* serverURL, const char* userId, const char* apiKey);

    void refreshData(int timestamp);
    bool isCanceled();
    bool isLocked(int timestamp);
    const char* getSessionId();
    int getStatus();

private:
    const char* serverURL;
    const char* userId;
    const char* apiKey;

    int lastUpdate;

    // Session infos
    char sessionId[30];
    int status;
    int startTimestamp;
    int endTimestamp;
    int minDuration;
    int maxDuration;

    HTTPClient* httpClient;
    void refreshSession(const char* sessionId);
    void getUserSession();

    char* buildURL(const char* path);
    char* buildURL(const char* path, char* parameters);
};
