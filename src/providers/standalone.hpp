#ifndef STANDALONE_HPP
#define STANDALONE_HPP

#include <WString.h>

void applyDuration(String durationString);
void applyRandomDuration(String durationFromString, String durationToString, bool showMessages);
int randomWithRisk(long randomFrom, long randomTo, bool showMessages);

#endif