This project permits to remote control a safe solenoid using an ESP32
By default it control a relay/transistor using the PIN 15

It has 2 modes : 
 * Emlalock controlled
 * Standalone mode

# Emlalock
Set your userId and apiKey in the configuration.h and you are good to go.
If you cancel a session the safe apply a punishement (EMLA_PUNISHMENT_AFTER_CANCEL_IN_MINUTES).
If your safe can't have any update from Emla for a defined time it will automatically open (EMLA_FAILSAFE_IN_MINUTES).

# Standalone mode
You can either just LOCK the safe, or set a timer.
Warning timer can't be reduced.
The lockee screen is useable without password for the lockee. He can only lock the safe (no unlock). If the safe is locked he can't set a timer (it would free him automatically at the end!).

# HTML Files
Don't forgert to send files in the SPIFFS:

```
platformio run --target uploadfs
````

# Wiring 
Look at /doc/wiring.png

I'm not using an arduino but a ESP32.
Don't forget to set the pin you used for the relay in configuration.h

# TODO
* emla failsafe is broken because it use "volatile" lastupdate value
** dont store last update for main loop, but store emlaLastUpdate in eeprom
** move handleEmla and handleManuel in providers
